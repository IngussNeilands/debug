<?php

namespace IngussNeilands\Debug;

class DebugItem {

    const BACKTRACE_ID = 2;

    protected $BackTrace;
    protected $ItemType;
    protected $Item;

    public function __construct($DebugItem = null) {

        $this->BackTrace = debug_backtrace();

        $this->ItemType = gettype($DebugItem);

        $this->Item = $DebugItem;
    }

    public function getType() {

        return $this->ItemType;
    }

    public function getItem() {

        return $this->Item;
    }

    public function getTrace() {

        return array_splice($this->BackTrace, 3);
    }

    public function getItemLocation() {

        if (isset($this->BackTrace[self::BACKTRACE_ID]['file']))
            return $this->BackTrace[self::BACKTRACE_ID]['file'] . ':' . $this->BackTrace[self::BACKTRACE_ID]['line'];
    }

}
