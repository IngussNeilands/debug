<?php

namespace IngussNeilands\Debug\Action;

class ActionGetD extends AbstractAction {

    public function getResponse($showItemLocation = true) {

        switch ($this->getDebugType()):

            case null:
                return $this->getItemLocation($showItemLocation)
                        . $this->getVarDumpContents();
                break;

            default:
                return $this->getItemLocation($showItemLocation)
                        . '<pre>' . print_r($this->getDebugItem()->getItem(), true) . '</pre>';
                break;

        endswitch;
    }

    protected function getVarDumpContents() {

        ob_start();
        call_user_func('var_dump', $this->getDebugItem()->getItem());
        $VarDumpContents = ob_get_contents();
        ob_end_clean();

        return $VarDumpContents;
    }

}
