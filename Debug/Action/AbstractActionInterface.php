<?php

namespace IngussNeilands\Debug\Action;

use IngussNeilands\Debug\DebugItem;

interface AbstractActionInterface {

    public function getDebugItem();

    public function getDebugType();

    public function getResponse($showItemLocation = true);

    public function getItemLocation($showItemLocation = true);
}
