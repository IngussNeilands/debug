<?php

namespace IngussNeilands\Debug\Action;

use IngussNeilands\Debug\DebugItem;
use Zend\Escaper\Escaper;

class ActionConsoleD extends AbstractAction {

    public function getResponse() {

        $DebugItem = $this->getDebugItem();

        return 'consoleD(): ' . $DebugItem->getItemLocation() . '\n\n'
                . $this->getEscaperResponse($DebugItem) . '\n\n';
    }

    public function getEscaperResponse(DebugItem $DebugItem) {

        $ZendEscaper = new Escaper('UTF-8');

        $Item = $DebugItem->getItem();

        if (is_object($Item) || is_array($Item))
            return $ZendEscaper->escapeJs(json_encode((array) $Item));

        return $ZendEscaper->escapeJs($Item);
    }

}
