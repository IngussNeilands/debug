<?php

namespace IngussNeilands\Debug\Action;

class ActionD extends AbstractAction {

    public function getResponse($showItemLocation = true) {

        switch ($this->getDebugType()):

            case null:
                echo $this->getItemLocation($showItemLocation);
                var_dump($this->getDebugItem()->getItem());
                break;

            default:
                echo $this->getItemLocation($showItemLocation)
                . '<pre>' . print_r($this->getDebugItem()->getItem(), true) . '</pre>';
                break;

        endswitch;
    }

}
