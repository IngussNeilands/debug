<?php

namespace IngussNeilands\Debug\Action;

use IngussNeilands\Debug\Action\AbstractActionInterface;
use IngussNeilands\Debug\DebugItem;

abstract class AbstractAction implements AbstractActionInterface {

    protected $DebugType;
    protected $DebugItem;

    public function __construct(DebugItem $DebugItem, $DebugType = null) {

        $this->setDebugItem($DebugItem);
        $this->setDebugType($DebugType);
    }

    protected function setDebugItem(DebugItem $DebugItem) {

        $this->DebugItem = $DebugItem;
    }

    protected function setDebugType($DebugType) {

        $this->DebugType = $DebugType;
    }

    public function getDebugItem() {

        return $this->DebugItem;
    }

    public function getDebugType() {

        return $this->DebugType;
    }

    public function getItemLocation($showItemLocation = true) {

        if (true == $showItemLocation)
            return '<i>' . $this->DebugItem->getItemLocation() . '</i>';

        return '';
    }

}
