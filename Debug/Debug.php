<?php

namespace IngussNeilands\Debug;

use IngussNeilands\Debug\SingleTon;
//
use IngussNeilands\Debug\DebugItem;
//
use IngussNeilands\Debug\Action\ActionConsoleD;
use IngussNeilands\Debug\Action\ActionGetD;
use IngussNeilands\Debug\Action\ActionD;

class Debug extends SingleTon {

    const ACTION_D = 'D';
    const ACTION_DD = 'DD';
    const ACTION_GETD = 'getD';
    const ACTION_DDTRACE = 'DDtrace';
    const ACTION_CONSOLED = 'consoleD';

    protected static $ConsoleLogDisplay = true;
    protected static $ShutdownFunction;
    protected $ConsoleItems = array();

    public function doAction($DebugAction, $DebugItemSource, $DebugType = null) {

        $DebugItem = new DebugItem($DebugItemSource);

        switch ($DebugAction):
            case self::ACTION_D:
                $DebugActionClass = new ActionD($DebugItem, $DebugType);
                return $DebugActionClass->getResponse();
                break;

            case self::ACTION_DD:
                $DebugActionClass = new ActionD($DebugItem, $DebugType);
                return $DebugActionClass->getResponse();
                break;

            case self::ACTION_GETD:
                $DebugActionClass = new ActionGetD($DebugItem, $DebugType);
                return $DebugActionClass->getResponse();
                break;

            case self::ACTION_DDTRACE:
                $DebugActionClass = new ActionD($DebugItem, $DebugType);
                echo $DebugActionClass->getResponse();
                $DebugActionClass = new ActionD(new DebugItem($DebugItem->getTrace()), $DebugType);
                return $DebugActionClass->getResponse(false);
                break;

            case self::ACTION_CONSOLED:
                $this->ConsoleItems[] = $DebugItem;

                if (empty(self::$ShutdownFunction))
                    self::$ShutdownFunction = $this->registerShutdownFunction();
                return;
                break;

        endswitch;
    }

    protected function registerShutdownFunction() {

        if ('cli' == php_sapi_name())
            return false;

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            return false;

        register_shutdown_function(array($this, 'callShutdownFunction'));

        return true;
    }

    public function callShutdownFunction() {

        if (empty($this->ConsoleItems))
            return;

        if (false == self::$ConsoleLogDisplay)
            return;

        $ConsoleLog = '\n';

        foreach ($this->ConsoleItems as $DebugItem):

            $DebugActionClass = new ActionConsoleD($DebugItem);
            $ConsoleLog.= $DebugActionClass->getResponse() . '\n';

        endforeach;

        echo '<script>console.log("' . $ConsoleLog . '" );</script>';
    }

    public static function hideConsole() {

        self::$ConsoleLogDisplay = false;
    }

}
