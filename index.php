<?php

error_reporting(E_ALL & ~E_STRICT);
ini_set('display_errors', true);

require 'vendor/autoload.php';

use IngussNeilands\Debug\Debug;

$ConsoleD = array('wtf' => 'consoleD');
$GetD = array('wtf' => 'getD');
$D = array('wtf' => 'D');

consoleD($ConsoleD);
consoleD($ConsoleD);
consoleD($ConsoleD);

// Optinally - hide All Console Logging
#Debug::hideConsole();

echo getD($GetD);
echo getD($GetD, 1);

D($D);
D($D, 1);

DD('fin');
