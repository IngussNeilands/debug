<?php

use IngussNeilands\Debug\Debug;

if (!function_exists('D')) {

    function D($DebugItem = null, $DebugType = null) {

        Debug::getInstance()->doAction(Debug::ACTION_D, $DebugItem, $DebugType);
    }

}

if (!function_exists('DD')) {

    function DD($DebugItem = null, $DebugType = null) {

        die(Debug::getInstance()->doAction(Debug::ACTION_DD, $DebugItem, $DebugType));
    }

}

if (!function_exists('getD')) {

    function getD($DebugItem = null, $DebugType = null) {

        return Debug::getInstance()->doAction(Debug::ACTION_GETD, $DebugItem, $DebugType);
    }

}

if (!function_exists('consoleD')) {

    function consoleD($DebugItem = 'null', $DebugType = null) {

        Debug::getInstance()->doAction(Debug::ACTION_CONSOLED, $DebugItem, $DebugType);
    }

}

if (!function_exists('DDtrace')) {

    function DDtrace($DebugItem = null, $DebugType = null) {

        die(Debug::getInstance()->doAction(Debug::ACTION_DDTRACE, $DebugItem, $DebugType));
    }

}